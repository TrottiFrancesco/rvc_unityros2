# UR5 ROS2 Unity simulator

In this guide you can find how to install, configure and use the simulator.
Simulator is based on two main components: Unity and ROS2.
Unity simulate the environment and the dynamics of the robot, while ROS2 is used to implement different algorithms.

## Requirements
- Ubuntu 20.04 LTS
- ROS2 Foxy or Galactic
- Unity 2020.30.31 LTS
- libnlopt-dev (download the library (https://nlopt.readthedocs.io/en/latest/) and build on your system)

If you have not installed something you can find some scripts in "doc/installation", in particular:

- install_ros2_foxy.sh to install ROS2 foxy and set the environment
- install_unityHub.sh to install Unity hub
- install_mono.sh for the Unity editor


## How simulator works

The simulator use a build-in ROS2 version in Unity and native ROS2; so the environment and robot dynamics are implemented in Unity while the algorithms are implements in ROS2. Unity having an internal ROS2 version is able to create publisher and subscriber nodes. 



## Repository

In this repository you can find three main folders:

- RoboticsVisionControl_1: this folder contains the first exercise (in sub-folder there is the documentation)
- RoboticsVisionControl_2: this folder contains the second exercise (in sub-folder there is the documentation)
- ROSNode: this folder contains the ROS2 nodes

I suggest to start setting the ROS2 node and after start with RoboticsVisionControl_1 and RoboticsVisionControl_2 
