using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rclcs;
using System;
public class JointController : MonoBehaviourRosNode
{
    public string topicName_0 = "ur5/command/joint_0";
    public string topicName_1 = "ur5/command/joint_1";
    public string topicName_2 = "ur5/command/joint_2";
    public string topicName_3 = "ur5/command/joint_3";
    public string topicName_4 = "ur5/command/joint_4";
    public string topicName_5 = "ur5/command/joint_5";

    
    public string frame_id = "";
    private Subscription<std_msgs.msg.Float32> sub_0;
    private Subscription<std_msgs.msg.Float32> sub_1;
    private Subscription<std_msgs.msg.Float32> sub_2;
    private Subscription<std_msgs.msg.Float32> sub_3;
    private Subscription<std_msgs.msg.Float32> sub_4;
    private Subscription<std_msgs.msg.Float32> sub_5;
    private Subscription<sensor_msgs.msg.JointState> sub;
    public string NodeName = "arm_joint_interface_node";
    protected override string nodeName { get { return NodeName; } }

    public ArticulationBody baseTreeArticulation;

    private ArticulationBody[] treeArticulation;
    public float[] joint_home_pose = {0,0,0,0,0,0};
    public float[] joint_pose = {0,0,0,0,0,0};
  

    // Start is called before the first frame update
    public void Start()
    {
        treeArticulation = baseTreeArticulation.GetComponentsInChildren<ArticulationBody>();
        initJointPose(treeArticulation);

    }
    protected override void StartRos()
    {

        sub_0 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_0, joint_0_callback);
        sub_1 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_1, joint_1_callback);
        sub_2 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_2, joint_2_callback);
        sub_3 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_3, joint_3_callback);
        sub_4 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_4, joint_4_callback);
        sub_5 = node.CreateSubscription<std_msgs.msg.Float32>(topicName_5, joint_5_callback);


    }

    public void initJointPose(ArticulationBody[] treeArticulation){

        joint_pose[0] = 0.0f;
        joint_pose[1] = -1.57f;
        joint_pose[2] = 1.57f;
        joint_pose[3] = 3.14f;
        joint_pose[4] = -1.57f;
        joint_pose[5] = -1.57f;
   
    }

    // Update is called once per frame
    public void Update()
    {   
        
        SpinSome();

        treeArticulation[0].jointPosition = new ArticulationReducedSpace(joint_pose[0], 0, 0);
        treeArticulation[1].jointPosition = new ArticulationReducedSpace(joint_pose[1], 0, 0);
        treeArticulation[2].jointPosition = new ArticulationReducedSpace(joint_pose[2], 0, 0);
        treeArticulation[3].jointPosition = new ArticulationReducedSpace(joint_pose[3], 0, 0);
        treeArticulation[4].jointPosition = new ArticulationReducedSpace(joint_pose[4], 0, 0);
        treeArticulation[5].jointPosition = new ArticulationReducedSpace(joint_pose[5], 0, 0);
        
    }


    private void joint_0_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[0] = msg.Data;
    }
    private void joint_1_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[1] = msg.Data;
    }
    private void joint_2_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[2] = msg.Data;
    }
    private void joint_3_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[3] = msg.Data;
    }
    private void joint_4_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[4] = msg.Data;
    }
    private void joint_5_callback(std_msgs.msg.Float32 msg)
    {
        joint_pose[5] = msg.Data;
    }
    void OnGUI() {
        int boundary = 20;

#if UNITY_EDITOR
        int labelHeight = 20;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 20;
#else
        int labelHeight = 40;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 40;
#endif
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
        for (int i = 0; i < 6; i++) {
            GUI.Label(new Rect(boundary, boundary + ( i * 2 + 1 ) * labelHeight, labelHeight * 4, labelHeight), "Joint " + i + ": ");
            joint_pose[i] = GUI.HorizontalSlider(new Rect(boundary + labelHeight * 4, boundary + (i * 2 + 1) * labelHeight + labelHeight / 4, labelHeight * 5, labelHeight), joint_pose[i], treeArticulation[i].xDrive.lowerLimit*Mathf.Deg2Rad, treeArticulation[i].xDrive.upperLimit*Mathf.Deg2Rad);
        }
    }

}
