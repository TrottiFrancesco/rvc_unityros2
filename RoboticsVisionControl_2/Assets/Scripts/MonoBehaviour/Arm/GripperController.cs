using System.Collections;
using System.Collections.Generic;
using rclcs;
using UnityEngine;

public class GripperController : MonoBehaviourRosNode
{
    // Start is called before the first frame update

    public string NodeName = "gripper_controller_node";
    private string topicName = "/wsg_50/controller/command";
    private Subscription<std_msgs.msg.Bool> sub;
    protected override string nodeName { get { return NodeName; } }
    public ArticulationBody finger_left;
    public ArticulationBody finger_right;
    public float[] finger_pose = {0,0};
    private bool status_gripper = false;
    float t, t1;
    float duration = 5.0f;
    void Start()
    {
        initGripperPose();

    }
    protected override void StartRos(){
        sub = node.CreateSubscription<std_msgs.msg.Bool>(topicName, gripper_callback);
    }

    private void initGripperPose(){
        finger_pose[0] = finger_left.xDrive.lowerLimit;
        finger_pose[1] = finger_right.xDrive.upperLimit;
        finger_left.jointPosition = new ArticulationReducedSpace(finger_pose[0], 0, 0);   
        finger_right.jointPosition = new ArticulationReducedSpace(finger_pose[1], 0, 0);      
    }

    // Update is called once per frame
    public void Update()
    {
        SpinSome();
        finger_left.jointPosition = new ArticulationReducedSpace(finger_pose[0], 0, 0);   
        finger_right.jointPosition = new ArticulationReducedSpace(finger_pose[1], 0, 0);   
        if(status_gripper){
            finger_pose[0] = Mathf.LerpAngle(finger_left.xDrive.lowerLimit, finger_left.xDrive.upperLimit, t);
            finger_pose[1] = Mathf.LerpAngle(finger_right.xDrive.upperLimit, finger_right.xDrive.lowerLimit, t);
            if (t <= 1) t += Time.deltaTime/duration;
            t1=0;
        }else{
            finger_pose[0] = Mathf.LerpAngle(finger_left.xDrive.upperLimit, finger_left.xDrive.lowerLimit, t1);
            finger_pose[1] = Mathf.LerpAngle(finger_right.xDrive.lowerLimit, finger_right.xDrive.upperLimit, t1);
            if (t1 <= 1) t1 += Time.deltaTime/duration;
            t = 0;
        }
    }


    private void gripper_callback(std_msgs.msg.Bool msg)
    {
        status_gripper = msg.Data;
    }
}
