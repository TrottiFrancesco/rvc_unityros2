using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rclcs;
using System;


public class CartesianController : MonoBehaviourRosNode
{
    public string topicName = "ur5/IK_joints";

    private Subscription<std_msgs.msg.Float32MultiArray> sub;
    public string NodeName = "cartesian_controller_node";
    protected override string nodeName { get { return NodeName; } }

    public ArticulationBody baseTreeArticulation;
    private ArticulationBody[] treeArticulation;
    public float[] joint_pose = {0,0,0,0,0,0};
    
    bool new_joint = true;


    // Start is called before the first frame update
    public void Start()
    {
        treeArticulation = baseTreeArticulation.GetComponentsInChildren<ArticulationBody>();
        initJointPose(treeArticulation);

        //jointControl = new JointController();
    }
    protected override void StartRos()
    {
        
        //treeArticulation = baseTreeArticulation.GetComponentsInChildren<ArticulationBody>();
        sub = node.CreateSubscription<std_msgs.msg.Float32MultiArray>(topicName, target_joint_callback);

        //sub = node.CreateSubscription<sensor_msgs.msg.JointState>("/joint_states", _callback);

    }

    public void initJointPose(ArticulationBody[] treeArticulation){

        joint_pose[0] = 0.0f;
        joint_pose[1] = -1.57f;
        joint_pose[2] = 1.57f;
        joint_pose[3] = 3.14f;
        joint_pose[4] = -1.57f;
        joint_pose[5] = -1.57f;

    }

    // Update is called once per frame
    public void Update()
    {   
        
        SpinSome();

        //if(new_joint){
            treeArticulation[0].jointPosition = new ArticulationReducedSpace(joint_pose[0], 0, 0);
            treeArticulation[1].jointPosition = new ArticulationReducedSpace(joint_pose[1], 0, 0);
            treeArticulation[2].jointPosition = new ArticulationReducedSpace(joint_pose[2], 0, 0);
            treeArticulation[3].jointPosition = new ArticulationReducedSpace(joint_pose[3], 0, 0);
            treeArticulation[4].jointPosition = new ArticulationReducedSpace(joint_pose[4], 0, 0);
            treeArticulation[5].jointPosition = new ArticulationReducedSpace(joint_pose[5], 0, 0);
            new_joint = false;
        //}
       
    }


    private void target_joint_callback(std_msgs.msg.Float32MultiArray msg)
    {
        joint_pose = msg.Data;
        new_joint = true;
    }

    void OnGUI() {
        int boundary = 20;

#if UNITY_EDITOR
        int labelHeight = 20;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 20;
#else
        int labelHeight = 40;
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = 40;
#endif
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
        for (int i = 0; i < 6; i++) {
            GUI.Label(new Rect(boundary, boundary + ( i * 2 + 1 ) * labelHeight, labelHeight * 4, labelHeight), "Joint " + i + ": ");
            joint_pose[i] = GUI.HorizontalSlider(new Rect(boundary + labelHeight * 4, boundary + (i * 2 + 1) * labelHeight + labelHeight / 4, labelHeight * 5, labelHeight), joint_pose[i], treeArticulation[i].xDrive.lowerLimit*Mathf.Deg2Rad, treeArticulation[i].xDrive.upperLimit*Mathf.Deg2Rad);
        }
    }

}
