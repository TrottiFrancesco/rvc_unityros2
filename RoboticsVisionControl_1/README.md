# RoboticsVisionControl 1

This folder contains the Unity project for the first exercise.

## Settings

To load this project you can:

    - open Unity Hub -> click on "open" button -> choose this folder and confirm

When Unity project starts you could see an interface like this:

![Unity env 1](/doc/img/ur5_1.PNG "Unity env")

Unity has three main parts: 
- Hierarchy: on the left side of the environment
- Browser: on the bottom of the environment
- Inspector: on the right side of the environment

Now it is necessary to have set correctly ROS2 node in your colcon workspace. If you didn't do it you can find the guide in ROS2Node folder

## Run simulator
If you click play button to run the simulation, the robot should move itself to the initial position.
If the configuration is correct, while Unity is running, you can open the command line and type this command to see the topics list:

``
ros2 topic list
``

If you see a list of topics, the environment is correctly configured and you can procede.

### Control type
Before you write your node you have to set the control type for the robot (modify the control loop with simulation stopped).
Actually, you can choose between a joint position controller or a cartesian position controller.
To enable or disable one of two controllers you have to:
- Select UR5 GameObject on the Hierarchy
- Go to Inspector side and move where all scripts are
- Between the scripts there should be a "Joint controller" and "Cartesian controller" scripts. If you enable or disable the check box on the left side of scripts name you switch the type of control.

![Unity scripts](/doc/img/Scripts.PNG "Unity scripts")

In parallel you always have a manual joints controller.
Using the slider on the left side of the scene you can modify the joints position (the manual joint control works only if there aren't any joints publisher)



### Aruco detection
Since the robot has a camera on the end-effector, the cube and target pose are identified through an aruco detection system.
When the robot will frame one or more aruco it will be colored in blue and its pose will be published; on the contrary, if aruco will not be framed the pose will not be published.
In the case more aruco were framed you should see something like this:

![Aruco detect](/doc/img/aruco_detect.png "Aruco detect")

### Topic list

When you run the Unity simulator and on command line type:

``
ros2 topic list
``

a list of topics like in this image should appear.

![Topics](/doc/img/topics.png "Topics")

Topics:

- /cube_\<color>/pose: this topic defines the pose of the cube. The pose will be published only when the camera frame the aruco. The message type is Pose, where are define the position and orientation of the cube expressed in robot base.
Unity publishes while ROS2 subscribes

- /dest_cube_\<color>/pose: this topic defines the target pose for the cube. The pose will be published only when the camera frame the aruco. The message type is Pose, where are define the position and orientation of the target position for the cube expressed in robot base.
Unity publishes while ROS2 subscribes

-  /joint_state: actual joints configuration. Unity publish while ROS2 subscribe

- /ur5/command/joint_\<number joint>: this topics allow to move the manipulator joints. If you publish on this topic the manipulator joints will move in target position. The message type is Float32 and the position is in radians. Unity subscribes ROS2 publishes

- /wsg_50/controller/command: this topic allow to controll the gripper. The message type is Bool. If you publish true the gripper will close, while if you publish false the gripper will open.
Unity subscribes ROS2 publishes

- /ur5/IK_joints: this is an internal topic, don't use it.


### ROS2 launch file

ROS2 side you have a package rvc_bringup, in the package you have two launch files in folder "launch".

- cartesian_bringup.launch.py: if in Unity you choose the cartesian controller in ROS2 you have to launch this launch file. When you run this launch file another topic will be created:
    - /ur5/ee_target/pose: this topic allows to receive target pose message for the cartesian controller. The message type is Pose and the pose has to be expressed in base frame
    - /ur5/ee_actual/pose: this topic allows to read the end-effector pose message for the joint controller. The message type is Pose and the pose have to be expressed in base frame

- joint_bringup.launch.py: if in Unity you choose the joint controller in ROS2 you have to launch this launch file. When you run this launch file another topic will be created:
    - /ur5/ee_actual/pose: this topic allows to read the end-effector pose message for the joint controller. The message type is Pose and the pose has to be expressed in base frame

## Your node

In order to move the robot correctly you have to create ROS2 node that implements the trajectory.
To do this you have to follow some steps:
- Create your ROS2 package. You can choose the language that you prefer: python or c++ 

- Define which topics you have to subscribe or publish

- Implement the main loop with the frequency that you want with the trajectory.

An example in cartesian space could be:

- Run simulator and launch the correct launch file for the cartesian control
- Define a middle point where the robot sees the cube and target 
- Subscribe to the cube and target topics to get the pose
- Subscribe to the actual robot end-effector pose topic
- Define a publisher on cartesian target points (/ur5/ee_target/pose)
- Implememnt a control loop with trajectory and publish a new point each iteration.
- Reach the position close scripper and move to the target cube pose

In joint space is the same but you have to publish the new position for each joints.

In the reference section you can find some tutorials.

## Reference

ROS2
- [Create ROS2 package](https://docs.ros.org/en/foxy/Tutorials/Creating-Your-First-ROS2-Package.html)
- [Create publisher subscriber node python](https://docs.ros.org/en/foxy/Tutorials/Writing-A-Simple-Py-Publisher-And-Subscriber.html)
- [Create publisher subscriber node c++](https://docs.ros.org/en/foxy/Tutorials/Writing-A-Simple-Cpp-Publisher-And-Subscriber.html)
