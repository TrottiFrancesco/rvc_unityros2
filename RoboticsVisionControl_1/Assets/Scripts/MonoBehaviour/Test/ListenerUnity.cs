using System.Collections;
using System.Collections.Generic;
using rclcs;
using UnityEngine;

public class ListenerUnity : MonoBehaviourRosNode
{
    // Start is called before the first frame update

    public string NodeName = "listener_unity";
    private string topicName = "unity_topic";
    private Subscription<std_msgs.msg.String> sub;
    protected override string nodeName { get { return NodeName; } }
    private string message;
    
    protected override void StartRos(){
        sub = node.CreateSubscription<std_msgs.msg.String>(topicName, topic_callback);
    }



    // Update is called once per frame
    public void Update()
    {
        SpinSome();

        
    }

    private void topic_callback(std_msgs.msg.String msg)
    {
        message = msg.Data;
        Debug.Log("Receive message " + message);
    }
}
