using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using rclcs;

public class TalkerUnity : MonoBehaviourRosNode
{
    public string NodeName = "talker_unity";
    protected override string nodeName { get { return NodeName; } }
    public string topicName = "unity_topic";
    public float ScanningFrequency = 10f;
    private std_msgs.msg.String text;
    private Publisher<std_msgs.msg.String> textPub;
    
    private int i = 0; 
    protected override void StartRos()
    {
        textPub = node.CreatePublisher<std_msgs.msg.String>(topicName);
        text = new std_msgs.msg.String();
    }

    // Update is called once per frame
    void Update()
    {
        
        text.Data = "Hallo from Unity " + i;
        textPub.Publish(text);
        i = i+1;
    }
    
}