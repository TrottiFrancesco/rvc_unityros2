using System.Collections;
using System.Collections.Generic;
using rclcs;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ArucoDetection : MonoBehaviourRosNode
{
    public string NodeName = "aruco_detection_node";
    protected override string nodeName { get { return NodeName; } }
    public Camera camera_detect;
    public GameObject camera_link;
    public GameObject obj;
    bool obj_dist = false;
    Color aruco_color;
    string aruco_name;
    public GameObject ref_frame;
    private Publisher<geometry_msgs.msg.PoseStamped> cube_pub;
    public string topicName = "";
    private geometry_msgs.msg.PoseStamped msg;
    public string index;

    // Start is called before the first frame update
    void Start()
    {
        aruco_color = new Color(0.0f, 0.0f, 1.0f, 0.5f);
    }

    protected override void StartRos(){
        cube_pub = node.CreatePublisher<geometry_msgs.msg.PoseStamped>(topicName);
        init_msg();
    }

    private void init_msg(){
        msg = new geometry_msgs.msg.PoseStamped();
        msg.Header.Frame_id = "";

    } 

    // Update is called once per frame
    private void Update()
    {
        SpinSome();   

        Vector3 objInCam = camera_detect.WorldToViewportPoint(obj.transform.position);


        float dist = Vector3.Distance(camera_link.transform.position, obj.transform.position);

        if(dist<0.5){
            obj_dist = true;
        }else{
            obj_dist = false;
        }
        bool onScreen = objInCam.z > 0 && objInCam.x > 0 && objInCam.x < 1 && objInCam.y > 0 && objInCam.y < 1 && obj_dist;

        aruco_name = "Aruco_" + index;
        
        bool obj_create = checkIsDraw(aruco_name);
        if(onScreen){
            if(!obj_create){
                drawObject(aruco_name);
            }
            update_msg();

        }else{
            if(obj_create){
                destroyObject(aruco_name);
            }
        }

    }
    private void update_msg(){
        msg.Header.Update(clock);
        msg.Pose.Unity2Ros(obj.transform.GetChild(0).transform, ref_frame.transform);
        cube_pub.Publish(msg);
    }

    private bool checkIsDraw(string aruco_id){
        List<GameObject> rootObjects = new List<GameObject>();
        Scene scene = SceneManager.GetActiveScene();
        scene.GetRootGameObjects(rootObjects);
        bool obj_is = false;
        for (int i = 0; i < rootObjects.Count; i++){
            GameObject gameObject = rootObjects[i];

            if(gameObject.name == aruco_id){
                obj_is =  true;
                break;
            }else{
                obj_is = false;
            }
        }
        return obj_is;
    }

    private void destroyObject(string aruco_id){
        List<GameObject> rootObjects = new List<GameObject>();
        Scene scene = SceneManager.GetActiveScene();
        scene.GetRootGameObjects(rootObjects);
        for (int i = 0; i < rootObjects.Count; i++){
            GameObject gameObject = rootObjects[i];
            if(gameObject.name == aruco_id){
                Destroy(gameObject);
                break;
            }
        }
    }
    private void drawObject(string aruco_name){

        var cube = GameObject.CreatePrimitive(PrimitiveType.Plane);
        cube.name = aruco_name;
        Renderer cube_render = cube.GetComponent<Renderer>();
        cube_render.material.color = aruco_color;
        cube_render.material.shader = Shader.Find( "Transparent/Diffuse" );
        cube.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.001f ,obj.transform.position.z);
        cube.transform.localScale = obj.transform.lossyScale;

    }   

}
