using System.Collections;
using System.Collections.Generic;
using rclcs;
using UnityEngine;

public class SpherePointPublisher : MonoBehaviourRosNode
{
    // Start is called before the first frame update
    public string NodeName = "sphere_point_node";
    protected override string nodeName { get { return NodeName; } }
    public GameObject ref_frame;
    private string topicName_1 = "/sphere_1/pose";
    private string topicName_2 = "/sphere_2/pose";
    private string topicName_3 = "/sphere_3/pose";
    private Publisher<geometry_msgs.msg.PoseStamped> pointPub_1;
    private Publisher<geometry_msgs.msg.PoseStamped> pointPub_2;
    private Publisher<geometry_msgs.msg.PoseStamped> pointPub_3;
    private geometry_msgs.msg.PoseStamped msg_1;
    private geometry_msgs.msg.PoseStamped msg_2;
    private geometry_msgs.msg.PoseStamped msg_3;
    Vector3 point;
    RaycastHit hit;
     private Vector3 posCur;
     float x,y,z, r;
     Quaternion rotation;
     
    private Quaternion rotCur;
    private GameObject obj;
    private List<Transform> point_list;
    private int count;
    
    protected override void StartRos()
    {
        pointPub_1 = node.CreatePublisher<geometry_msgs.msg.PoseStamped>(topicName_1);
        pointPub_2 = node.CreatePublisher<geometry_msgs.msg.PoseStamped>(topicName_2);
        pointPub_3 = node.CreatePublisher<geometry_msgs.msg.PoseStamped>(topicName_3);
        init_msgs();
    }
    // Update is called once per frame
    private void init_msgs(){
        msg_1 = new geometry_msgs.msg.PoseStamped();
        msg_2 = new geometry_msgs.msg.PoseStamped();
        msg_3 = new geometry_msgs.msg.PoseStamped();
        msg_1.Header.Frame_id = "";
        msg_2.Header.Frame_id = "";
        msg_3.Header.Frame_id = "";

    }
    void Start()
    {
        point_list = new List<Transform>(3);
    }

    // Update is called once per frame
    void Update()
    {
        SpinSome();
        msg_1.Header.Update(clock);
        msg_2.Header.Update(clock);
        msg_3.Header.Update(clock);
        if(count<3){
            if( Input.GetMouseButtonDown(0) )
            {
                Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
                RaycastHit hit;
                
                if( Physics.Raycast( ray, out hit, 100 ) )
                {
                    string name = hit.transform.gameObject.name;
                    if(name == "Sphere_1" || name == "Sphere_2" || name == "Sphere_3" || name == "Sphere_4" || name == "Sphere_5"){
                        obj=hit.transform.gameObject;
                        obj.GetComponent<Renderer>().material.color = Color.blue;
                        Collider col = obj.GetComponent<Collider>();
                        point_list.Add(obj.transform);
                        count++;
                        col.enabled = false;
                    }
                    
                }
            }
        }else{
            msg_1.Pose.Unity2Ros(point_list[0].GetChild(0).transform, ref_frame.transform);
            msg_2.Pose.Unity2Ros(point_list[1].GetChild(0).transform, ref_frame.transform);
            msg_3.Pose.Unity2Ros(point_list[2].GetChild(0).transform, ref_frame.transform);
            //msg_3.Pose.Position.Z = msg_3.Pose.Position.Z + 0.12f; 
            pointPub_1.Publish(msg_1);
            pointPub_2.Publish(msg_2);
            pointPub_3.Publish(msg_3);
        }


        
    }
}
