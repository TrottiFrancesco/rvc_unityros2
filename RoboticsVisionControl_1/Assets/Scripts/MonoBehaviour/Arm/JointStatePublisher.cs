using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using rclcs;

public class JointStatePublisher : MonoBehaviourRosNode
{
    public ArticulationBody articulationRobot;
    public string NodeName = "joint_states_node";
    protected override string nodeName { get { return NodeName; } }
    public string topicName = "joint_states";
    public string frame_id = "";
    public float ScanningFrequency = 10f;
    private sensor_msgs.msg.JointState joint_msgs;
    private Publisher<sensor_msgs.msg.JointState> jointStatePub;
    public string[] jointForJointState =
    {   "robot_arm_shoulder_pan_joint",
        "robot_arm_shoulder_lift_joint",
        "robot_arm_elbow_joint",
        "robot_arm_wrist_1_joint",
        "robot_arm_wrist_2_joint",
        "robot_arm_wrist_3_joint",
        "robot_wsg50_finger_left_joint",
        "robot_wsg50_finger_right_joint"};
    
    public List<string> linkForJointState = new List<string>
    {   "robot_arm_shoulder_link",
        "robot_arm_upper_arm_link",
        "robot_arm_forearm_link",
        "robot_arm_wrist_1_link",
        "robot_arm_wrist_2_link",
        "robot_arm_wrist_3_link",
        "robot_wsg50_gripper_left",
        "robot_wsg50_gripper_right"};

    private ArticulationBody[] jointListUrdf;
    // Start is called before the first frame update

    protected override void StartRos()
    {
        init_msgs();
        jointStatePub = node.CreatePublisher<sensor_msgs.msg.JointState>(topicName);
        jointListUrdf = new ArticulationBody[linkForJointState.Count];
        getJointFromArr();
        //StartCoroutine("JointPublish");
    }
    // IEnumerator JointPublish()
    // {
    //     for (;;)
    //     {
    //         jointStatePub.Publish(joint_msgs);
    //         yield return new WaitForSeconds(0.1f / ScanningFrequency);
    //     }
    // }
    private void init_msgs()
    {   
        joint_msgs = new sensor_msgs.msg.JointState();
        int len_joint = jointForJointState.Length;
        joint_msgs.Header.Frame_id = frame_id;
        joint_msgs.Name = new string[len_joint];
        joint_msgs.Position = new double[len_joint];
        joint_msgs.Velocity = new double[len_joint];
        joint_msgs.Effort = new double[len_joint];
    }

    private void updateMsgJoint(int i, ArticulationBody joint)
    {
        joint_msgs.Velocity[i] = joint.jointVelocity[0];
        joint_msgs.Effort[i] = joint.jointForce[0];
        joint_msgs.Position[i] = joint.jointPosition[0];
    }
    // Update is called once per frame
    void Update()
    {
        
        joint_msgs.Header.Update(clock);
        getJointFromArr();

        for(int i = 0; i < jointListUrdf.Length; i++)
        {
            updateMsgJoint(i, jointListUrdf[i]);
        }
        joint_msgs.Name = jointForJointState;
        jointStatePub.Publish(joint_msgs);
    }
    private void getJointFromArr()
    {
        
        ArticulationBody[] artjointArr = articulationRobot.GetComponentsInChildren<ArticulationBody>();
        for(int i = 0; i < linkForJointState.Count; i++)
        {
            for (int j = 0; j < artjointArr.Length; j++)
            {
                
                if (artjointArr[j].jointType != ArticulationJointType.FixedJoint)
                {
                    if (artjointArr[j].name.ToString().Contains(linkForJointState[i]))
                    {
                        jointListUrdf[i] = artjointArr[j];
                    }
                }
            }
        }

    }
}