using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraspController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cube;
    public GameObject parent_obj;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionStay(Collision collision)
    {

        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "robot_wsg50_finger_right")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            cube.transform.parent = parent_obj.transform;
            //robot.GetComponent<GripperController>().lock_gripper = true;

        }
        
    }
    void OnCollisionEnter(Collision collision)
    {

        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "robot_wsg50_finger_right")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            cube.transform.parent = parent_obj.transform;
            //robot.GetComponent<GripperController>().lock_gripper = true;

        }
        
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.name == "robot_wsg50_finger_right")
        {
            cube.transform.SetParent(null);
            //cube.GetComponent<Rigidbody>().isKinematic = false;
            
        }

    }
    public void disableKinematic(){
        cube.GetComponent<Rigidbody>().isKinematic = false;
        cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }
    public void enableKinematic(){
        cube.GetComponent<Rigidbody>().isKinematic = true;
        cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
}
