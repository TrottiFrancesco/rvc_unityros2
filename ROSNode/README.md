# ROSNode
This folder contains the ROS2 package to move the robot

## Settings

- Open folder and copy the packages in your colcon workspace (colcon_ws/src)
- Move to your colcon workspace 
- Build trac-ik package (colcon build --packages-select trac_ik_lib)
- Source your envornment (. install/setup.bash)
- Build all packages (colcon build)
- Source your envornment (. install/setup.bash)

From the package that you have imported one is important for the exercises: rvc_bringup.

In this package you can find different folders:
- include: directory for header file
- src: directory for source file
- launch: directory for launch file

Actually, only the launch directory is important.
In partucolar, in this folder you can find two main files:
- cartesian_bringup.launch.py: this launch file enable the cartesian control and realtive code solve the inverse kinematic and forward kineamatic of the robot
- joint_bringup.launch.py: this launch file enable the joints control and realtive code solve forward kineamatic of the robot
